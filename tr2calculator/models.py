from django.db import models


# > python manage.py makemigrations
# > python manage.py migrate
# Warning: You'll need to run these commands every time 
# your models change in a way that will affect the structure of 
# the data that needs to be stored 
# (including both addition and removal of whole models and individual fields).


# Create your models here.
class AxomatricsLog(models.Model):
    pass


class Condition(models.Model):
    pass


class OpticsLog(models.Model):
    pass
